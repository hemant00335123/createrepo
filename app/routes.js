// app/routes.js
var NewUser       = require('./models/newuser');
var user          =require('./models/user');
var nodemailer = require("nodemailer");
var { TRANSPORTER_OPTIONS, SENDER } = require("../config/mailer");

module.exports = function(app, passport) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		res.redirect('/fusion/rpo') // load the index.ejs file
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('login.ejs', { message: req.flash('loginMessage') });
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
		successRedirect : '/newuser/view', // redirect to the secure profile section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {

		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/newuser/view', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		res.render('profile.ejs', {
			user : req.user // get the user out of session and pass to template
		});
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================

	app.get('/logout', function(req, res) {
		req.session.destroy(function (err) {
			res.redirect('/');
		})
	});



app.get('/fusion/rpo', function(req, res) {
    res.render('signup1.ejs', {
        user : req.user
    });
});


app.post('/fusion/rpo', function(req, res){
    var user = new NewUser()
    user.name = req.body.name;
    user.emailid = req.body.emailid;
    user.message = req.body.message;
		user.MobileNo = req.body.MobileNo;
		user.companyname= req.body.companyname;

    user.save(function(err, doc){
      if(err){
        res.json(err);
      }
      else{
				var maillist = 'nextpixar.com@gmail.com';
				process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
				var transporter = nodemailer.createTransport(TRANSPORTER_OPTIONS);
				var transporter1 = nodemailer.createTransport(TRANSPORTER_OPTIONS);
				var mailOptions = {
					from: SENDER , // sender address
					to: maillist , // list of receivers
					subject: 'New Form', // Subject line
					html: '<div><h1>User contect From</h1><div style="width: 100%;float: left;"><div style="width: 50%;float: left;"> Name</div><div style="width: 50%;float: left;"> '+req.body.name +'</div></div><div style="width: 100%;float: left;"><div style="width: 50%;float: left;"> Contect NUmber</div><div style="width: 50%;float: left;">'+ req.body.MobileNo +' </div></div></div><div style="width: 100%;float: left;"><div style="width: 50%;float: left;"> Message</div><div style="width: 50%;float: left;">'+ req.body.message + ' </div></div><div style="width: 100%;float: left;"><div style="width: 50%;float: left;">Email Address</div><div style="width: 50%;float: left;">'+ req.body.emailid + ' </div></div></div>' // You can choose to send an HTML body instead
				};
				var mailOptions1 = {
					from: SENDER , // sender address
					to: req.body.emailid , // list of receivers
					subject: 'New Form', // Subject line
					html: '<div>Hi your mail send Successfully</div>' // You can choose to send an HTML body instead
				};
				transporter.sendMail(mailOptions, function(error, info){
					if(error){
						console.log('error: ', error);

					}else{
						req.flash('passwordForgotSuccess','Mail sent successfully')
						res.redirect('/password/forgot');
					}
				});
				transporter1.sendMail(mailOptions1, function(error, info){
					if(error){
						console.log('error: ', error);

					}else{
						req.flash('passwordForgotSuccess','Mail sent successfully')

					}
				});
        console.log(user._id);
    	res.redirect('/fusion/thankyou');
      }
    });
  });


  app.get('/newuser/view', function(req, res) {
	NewUser.find({},function(err,foundApplicants){
	  res.render('userview.ejs', {
		  user : req.user,
		  NewUserdata : foundApplicants
	  });
	});
	});

	app.get('/welcomeuser', function(req, res) {
		res.render('welcomeuser.ejs'); // load the index.ejs file
	});

  app.get('/newuser/delete/:id', function (req, res) {
	NewUser.deleteOne({_id : req.params.id }, function(err, obj) {
		if (err) throw err;
		console.log("1 document deleted");
		res.redirect('/newuser/view')
	  });
	});
	app.get('/newuser/view/:id', function (req, res) {
		NewUser.find({_id : req.params.id }, function(err, obj) {
			if (err) throw err;
			console.log(" document view");

			});
		});

		app.get('/newuser/update/:id', function(req, res) {
			NewUser.findOne({_id : req.params.id},function(err,foundApplicants){
				res.render('updateview.ejs', {
					user : req.user,
					NewUser : foundApplicants,

        detailError : req.flash('profileDetailError'),
        detailSuccess : req.flash('profileDetailSuccess'),
        photoError : req.flash('profilePhotoError'),
        photoSuccess : req.flash('profilePhotoSuccess')
				});
			});
			});


app.post('/edit/details', function(req, res) {
	NewUser.updateOne({_id : req.body._id},{
	  $set : {
		username : req.body.username,
		fullname : req.body.fullname,
		emailid : req.body.emailid,
		State : req.body.State,
		Address : req.body.Address,
		MobileNo : req.body.MobileNo


	  }},
	  function(err, result){
		if(err) {
		  console.log(err);
		 // req.flash('profileDetailError','An error occured. Please try again!');
		  res.redirect('/newuser/view');
		}
		else {
		 // req.flash('profileDetailSuccess','Your details were changed successfully!');
		  res.render('welcomeuser.ejs');
		}
	  })
	});



	app.get('/fusion/thankyou', function(req, res) {
		res.render('welcome.ejs'); // load the index.ejs file
	});


};



// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();

	// if they aren't redirect them to the home page
	res.redirect('/');
}
