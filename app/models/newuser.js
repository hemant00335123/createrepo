var mongoose = require('mongoose');

// define the schema for our user model
var newyserSchema = mongoose.Schema({

  name   : String,
  emailid : String,
  message : String,
  MobileNo : String,
  companyname: String
});


// create the model for users and expose it to our app
module.exports = mongoose.model('newuser', newyserSchema);