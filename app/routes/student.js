var app = require('express').Router();


var User       = require('../models/user');


var NewUser       = require('../models/newuser');


app.get('/newuser/create', function(req, res) {
    res.render('newuser.ejs', {
        user : req.user
    });
});


app.post('/newuser/create', function(req, res){
    var user = new NewUser()
    user.username = req.body.username;
    user.FullName = req.body.FullName;
    user.State = req.body.State;
    user.emailid = req.body.emailid;
    user.Address = req.body.Address;
    user.MobileNo = req.body.MobileNo;
    user.save(function(err, doc){
      if(err){
        res.json(err);
      }
      else{
        console.log(user._id);
        res.redirect('/student/newuser/create')
      }
    });
  });



  module.exports = app;